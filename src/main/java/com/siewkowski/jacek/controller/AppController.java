package com.siewkowski.jacek.controller;

import com.siewkowski.jacek.model.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jacek Siewkowski on 11.05.2016.
 */
@Controller
public class AppController {

    //tworzymy listę z wartosciami do wykresu i pusty obiekt transfer, ladujemy je do mapy przez co ida do home.html
    // pod nazwami data i transfer
    @RequestMapping(value = "/")
    public String home(ModelMap modelMap) {

        PostgresqlDB.PostgresqlDBInitialize();
        List<Data> list = PostgresqlDB.getRange("1998-01-05", "2016-05-12");
        modelMap.put("data", list);
        Transfer transfer = new Transfer();
        modelMap.put("transfer", transfer);
        return "home";
    }

    //odbieramy obiekt tranfer z wartościami z htmla, tworzymy na jego podstawie nowa liste z wartościami do wykresu
    // z powrotem razem z obiektem transfer ładujemy oba obiekty do mapy
    @RequestMapping(value = "/compare")
    public String compare(@ModelAttribute(value = "transfer") Transfer transfer, ModelMap modelMap) {
        List<Data> list;

        String start = transfer.getDateBegin();
        String stop = transfer.getDateEnd();

        int compare = start.compareTo(stop);
        if (compare > 0 ) list = PostgresqlDB.getRange(start,stop);
        else list = PostgresqlDB.getRange(start, stop);
        List<CompareTable> array = prepareCompareTable(list, transfer);
        modelMap.put("transfer", transfer);
        modelMap.put("data", array);
        return "compare";
    }

    private List<CompareTable> prepareCompareTable(List<Data> list, Transfer transfer) {
        List<CompareTable> lists = new ArrayList<>();
        BigDecimal a = transfer.getInvestmentAmount();
        BigDecimal b = transfer.getInvestmentAmount();
        BigDecimal c = transfer.getDepositPercentage();
        BigDecimal d = a.multiply(c.divide(BigDecimal.valueOf(100))).divide(BigDecimal.valueOf(list.size()), 2, BigDecimal.ROUND_HALF_UP);

        for (int i = 0; i < list.size() - 1; i++) {
            b = b.multiply(list.get(i + 1).getValue()).divide(list.get(i).getValue(), 2, BigDecimal.ROUND_HALF_UP);
            a = a.add(d);
            lists.add(new CompareTable(list.get(i).getDate(), b, a));
        }
        return lists;
    }
}