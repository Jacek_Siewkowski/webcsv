package com.siewkowski.jacek.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jacek Siewkowski on 27.05.2016.
 */
public class SQLiteDB {

    private static Connection connection;
    private static Statement statement;
    private static SQLiteDB DB;

    static {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private SQLiteDB() throws SQLException, IOException, ParseException, ClassNotFoundException {
        getConnection();
        BufferedReader br = new BufferedReader(new FileReader("/data.csv"));
        statement.executeUpdate("DROP TABLE IF EXISTS data");
        statement.executeUpdate("CREATE TABLE data (id INTEGER PRIMARY KEY, date TEXT, value REAL)");
        //seeding
        //omitting the header in csv
        String line = line = br.readLine();
        int count = 0;
        int batchsize = 1000;
        while ((line = br.readLine()) != null) {
            String[] values = line.split(",");
            String cmd = getSQLCmd(values);
            statement.addBatch(cmd);
            if (++count % batchsize == 0) statement.executeBatch();
        }
        statement.executeBatch();
//        closeConnection();
    }

    private static void getConnection() {
        try {
            String string = "jdbc:sqlite:file:./src/main/resources/data.db";
            connection = DriverManager.getConnection(string);
            connection.setAutoCommit(false);
            statement = connection.createStatement();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void closeConnection() {
        try {
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private String getSQLCmd(String[] values) throws ParseException {
        String date = getProperDate(values[0]);
        BigDecimal val = new BigDecimal(values[1]);
        String sql = "INSERT INTO data (date, value) VALUES('%s'," + val + ")";
        return String.format(sql, date);
    }

    private String getProperDate(String date) {
        String[] tab = date.split("/");
        return tab[2] + "-" + tab[1] + "-" + tab[0];

    }

    public static void  initializeDB() {
        if (DB == null) try {
            DB = new SQLiteDB();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static List<Data> getRange(String start, String stop) {
//        getConnection();
        List<Data> list = new ArrayList<>();
        ResultSet rs = null;
        String cmd = String.format("SELECT * FROM data WHERE date BETWEEN '%s' AND '%s'", start, stop);
        System.out.printf(cmd + "%n");
        try {
            rs = statement.executeQuery(cmd);
            while (rs.next()) {
                int id = rs.getInt("id");
                Date date = rs.getDate("date");
                BigDecimal value = rs.getBigDecimal("value");
                list.add(new Data(date.toString(), value));
                System.out.printf("%s %n",date);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        closeConnection();
        return list;

    }
}

