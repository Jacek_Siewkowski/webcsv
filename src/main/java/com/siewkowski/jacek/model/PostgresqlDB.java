package com.siewkowski.jacek.model;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jacek Siewkowski on 03.06.2016.
 */
public class PostgresqlDB {

    private static Connection connection;
    private static Statement statement;
    private static PostgresqlDB postgresqlDB;

    private PostgresqlDB() {

        try {
            Class.forName("org.postgresql.Driver");
            //do Amazona AWS
//           connection = DriverManager.getConnection("jdbc:postgresql://postdb.cq55monghezt.us-west-2.rds.amazonaws.com:5432/postdb","postgres", "Unclean666");

            //do Google Cloud Platform
            connection = DriverManager.getConnection("jdbc:postgresql://104.155.28.63:5432/postgres", "postgres", "GQ9giHkB");
            statement = connection.createStatement();
            statement.executeUpdate("DROP TABLE IF EXISTS data");
            statement.executeUpdate("CREATE TABLE data (id SERIAL PRIMARY KEY, date DATE, value DECIMAL)");

            // w przypaku gdy plik jest u nas na dysku
//            File file = new File("src\\main\\resources\\data.csv");
//            String filePath = file.getAbsolutePath();
//            String copCmd = String.format("COPY data(date,value) FROM '%s' DELIMITERS ',' CSV HEADER", filePath);

            //bez ustawienia formatu nie przejdzie
            statement.executeUpdate("SET datestyle = 'ISO, DMY'");
            String copCmd = "COPY data(date,value) FROM '/home/file/data.csv' DELIMITERS ',' CSV HEADER";

            statement.executeUpdate(copCmd);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void PostgresqlDBInitialize() {

        if (postgresqlDB == null) postgresqlDB = new PostgresqlDB();

    }

    public static List<Data> getRange(String start, String stop) {
        List<Data> list = new ArrayList<>();
        ResultSet rs = null;
        String cmd = String.format("SELECT * FROM data WHERE date BETWEEN '%s' AND '%s'", start, stop);
        System.out.printf(cmd + "%n");
        try {
            rs = statement.executeQuery(cmd);
            while (rs.next()) {
                int id = rs.getInt("id");
                String date = rs.getString("date");
                BigDecimal value = rs.getBigDecimal("value");
                list.add(new Data(date, value));
                System.out.printf("%s %n", date);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (Data d : list)
            System.out.println(d.getDate() + " " + d.getValue());
        return list;

    }
}
