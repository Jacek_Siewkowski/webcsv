package com.siewkowski.jacek.model;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by Jacek Siewkowski on 29.05.2016.
 */
public class CompareTable {
    public CompareTable() {
    }

    private String date;
    private BigDecimal valueFunds;
    private BigDecimal valueDeposit;

    public CompareTable(String date, BigDecimal valueFunds, BigDecimal valueDeposit) {
        this.date = date;
        this.valueFunds = valueFunds;
        this.valueDeposit = valueDeposit;
    }

    public String getDate() {

        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public BigDecimal getValueFunds() {
        return valueFunds;
    }

    public void setValueFunds(BigDecimal valueFunds) {
        this.valueFunds = valueFunds;
    }

    public BigDecimal getValueDeposit() {
        return valueDeposit;
    }

    public void setValueDeposit(BigDecimal valueDeposit) {
        this.valueDeposit = valueDeposit;
    }


}
