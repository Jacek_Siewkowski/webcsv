package com.siewkowski.jacek.model;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;

/**
 * Created by Jacek Siewkowski on 29.05.2016.
 */
public class Transfer {
    private String dateBegin;
    private String dateEnd;
    private BigDecimal investmentAmount;
    private BigDecimal depositPercentage;

    public Transfer() {
    }

    public Transfer(String dateBegin, String dateEnd, BigDecimal investmentAmount, BigDecimal depositPercentage) {

        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
        this.investmentAmount = investmentAmount;
        this.depositPercentage = depositPercentage;
    }

    public String getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(String dateBegin) {
        this.dateBegin = dateBegin;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public BigDecimal getInvestmentAmount() {
        return investmentAmount;
    }

    public void setInvestmentAmount(BigDecimal investmentAmount) {
        this.investmentAmount = investmentAmount;
    }

    public BigDecimal getDepositPercentage() {
        return depositPercentage;
    }

    public void setDepositPercentage(BigDecimal depositPercentage) {
        this.depositPercentage = depositPercentage;
    }
}
