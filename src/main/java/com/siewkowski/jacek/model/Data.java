package com.siewkowski.jacek.model;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * Created by Jacek Siewkowski on 27.05.2016.
 */
public class Data {
    private String date;
    private BigDecimal value;

    public Data() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }


    public Data(String date, BigDecimal value) {

        this.date = date;
        this.value = value;
    }


}
